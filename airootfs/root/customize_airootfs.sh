#!/bin/bash

set -e -u

sed -i 's/# \(%wheel ALL=(ALL) ALL\)/\1/' /etc/sudoers
echo "Defaults !env_reset" >> /etc/sudoers

useradd -m -g users -G wheel -s /usr/bin/zsh firerain
passwd -d firerain

systemctl enable NetworkManager.service sddm.service
systemctl set-default graphical.target
