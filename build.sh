#!/usr/bin/bash

_buildsh_path="$(realpath -- "$0")"

exec mkarchiso "$@" "${_buildsh_path%/*}"
